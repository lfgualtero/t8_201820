package controller;

import api.IVertice;
import model.data_structures.listas.LinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VODistance;
import model.vo.VOInterseccion;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void guardarGrafoEnFormatoJSON() {
		manager.guardarGrafoJSON("./data/grafoDefinitivo.json");
	}
	
	public static void cargarGrafoEnFormatoJSON() {
		manager.cargarGrafoJSON("./data/grafoDefinitivo.json");;
	}

	
	public static void loadStations() {
		manager.loadStations("./data/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadIntersections() {
		manager.loadIntersection("./data/Nodes_of_Chicago_Street_Lines.txt");
	}
	
	public static void loadEdges() {
		manager.loadEdges("./data/Adjacency_list_of_Chicago_Street_Lines.txt");
	}
	
	public static int vertices() {
		return manager.getGrafo1().getVertices().sizeKeys();
	}
	
	public static int arcos() {
		return manager.getGrafo1().getArcos().sizeKeys();
	}
	
	public static void agregarEstacionesGrafo() {
		manager.addStationsToGraph();
	}
	public static int getArcosMixtos() {
		return manager.getArcosMixtos();
	}
	
	public static int getVerticesEstacion() {
		return manager.getArcosMixtos();
	}
}
