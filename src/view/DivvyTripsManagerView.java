package view;

import java.util.Scanner;



import controller.Controller;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadIntersections();
					System.out.println("Vertices anadididos: "+Controller.vertices());
					break;
					
				case 2:
					Controller.loadEdges();
					System.out.println("Arcos anadididos: "+Controller.arcos());
					break;
					
				case 3:
					Controller.loadStations();
					Controller.agregarEstacionesGrafo();
					System.out.println("Arcos actuales: "+Controller.arcos());
					System.out.println("Vertices actuales: "+Controller.vertices());
					System.out.println("Arcos mixtos: "+Controller.getArcosMixtos());
					System.out.println("Vertices estacion actuales: "+(Controller.getVerticesEstacion()));
					break;
					
				case 4:
					
					Controller.guardarGrafoEnFormatoJSON();
					System.out.println("Revise su archivo en la carpeta ./data");
					
					break;
				case 5:
					Controller.cargarGrafoEnFormatoJSON();
					break;
					
				case 6:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("1. Cargue las intersecciones al grafo");
		System.out.println("2. Cargue los arcos al grafo");
		System.out.println("3. Agregue las estaciones al grafo");
		System.out.println("4. Guarde el grafo en archivo Json");
		System.out.println("5. Cargue el grafo de un archivo Json");
		System.out.println("6. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
