package model.data_structures.grafo1;

import java.util.Iterator;

import api.IArco;
import api.IGrafo;
import api.IVertice;
import model.data_structures.HashTables.HashTable;
import model.data_structures.listas.LinkedList;

public class Grafo <K extends Comparable<K>, V extends IVertice<K>, A extends IArco> implements IGrafo<K, IVertice<K>, IArco>{

	private HashTable<K, V> vertices;

	private HashTable<String, A> arcos;

	private HashTable<K, LinkedList<K>> listaAdj;

	/**
	 * Crea un grafo No dirigido sin v�rtices y sin arcos
	 */
	public Grafo ( ){
		vertices= new HashTable<K, V>(1000);
		arcos= new HashTable<String, A>(1000);
		listaAdj= new HashTable<K, LinkedList<K>>(1000);
	}

	public HashTable<K, V> getVertices() {
		return vertices;
	}
public Iterator darVerticesKeys(){
	return vertices.keysIterator();
}
	public HashTable<String, A> getArcos() {
		return arcos;
	}

	public HashTable<K, LinkedList<K>> getListaAdj() {
		return listaAdj;
	}

	/**
	 *N�mero de v�rtices
	 */
	@Override
	public int V() {
		return vertices.sizeKeys();
	}

	/**
	 * N�mero de arcos. Cada arco No dirigido debe contarse una �nica vez.
	 */
	@Override
	public int E() {
		return arcos.sizeKeys();
	}

	/**
	 * Adiciona un v�rtice con un Id �nico. El v�rtice tiene la informaci�n InfoVertex
	 * @throws VerticeNoExisteException 
	 */
	public void addVertex(K idVertex, V infoVertex) throws VerticeNoExisteException {
		if(infoVertex!=null){
			vertices.put(idVertex,  infoVertex);
		}else{
			throw new VerticeNoExisteException("El vertice a a�adir es nulo", idVertex);
		}
	}

	/**
	 * Adiciona el arco No dirigido entre el vertice IdVertexIni y el vertice IdVertexFin. El arco tiene la informaci�n infoArc.
	 * @throws ArcoNoExisteException 
	 */

	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) throws ArcoNoExisteException {
		if(infoArc!=null){
			arcos.put(""+idVertexIni+idVertexFin+"", infoArc);
		}else{
			throw new ArcoNoExisteException("El arco a a�adir es nulo", idVertexIni, idVertexFin);
		}
	}

	/**
	 * Obtener la informaci�n de un v�rtice
	 */
	@Override
	public IVertice<K> getInfoVertex(K idVertex) {
		return vertices.get(idVertex);
	}

	/**
	 * Modificar la informaci�n del v�rtice idVertex
	 * @throws VerticeNoExisteException 
	 */
	public void setInfoVertex(K idVertex, V infoVertex) throws VerticeNoExisteException {
		if(infoVertex!=null){
			if(vertices.existsKey(idVertex)){
				vertices.put(idVertex, infoVertex);
			}else{
				throw new VerticeNoExisteException("El vertice no est� a�adido", idVertex);
			}
		}else{
			throw new VerticeNoExisteException("El vertice por a�adir es nulo", idVertex);
		}
	}

	/**
	 * Obtener la informaci�n de un arco 	
	 * @throws ArcoNoExisteException 
	 */
	@Override
	public IArco getInfoArc(K idVertexIni, K idVertexFin) throws ArcoNoExisteException {

		if(arcos.existsKey(""+idVertexIni+idVertexFin+"")){
			return arcos.get(""+idVertexIni+idVertexFin+"");
		}else{
			throw new ArcoNoExisteException("El arco no existe", idVertexIni, idVertexFin);
		}
	}

	/**
	 * Obtener la informaci�n de un arco
	 * @throws ArcoNoExisteException 
	 */
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) throws ArcoNoExisteException {
		if(infoArc!=null){
			if(arcos.existsKey(""+idVertexIni+idVertexFin+"")){
				arcos.put(""+idVertexIni+idVertexFin+"", infoArc);
			}else{
				throw new ArcoNoExisteException("El arco no est� a�adido", idVertexIni, idVertexFin);
			}
		}else{
			throw new ArcoNoExisteException("El arco por a�adir es nulo", idVertexIni, idVertexFin);
		}
	}

	/**
	 * Retorna los identificadores de los v�rtices adyacentes a idVertex
	 * @throws VerticeNoExisteException 
	 */
	@Override
	public Iterator<K> adj(K idVertex) throws VerticeNoExisteException {
		if(listaAdj.existsKey(idVertex)){
			LinkedList<K> l= (LinkedList<K>) listaAdj.get(idVertex);
			return  l.iterator();
		}else{
			throw new VerticeNoExisteException("El vertice no existe en la lista", idVertex);
		}
	}


}
