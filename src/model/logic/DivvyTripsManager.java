package model.logic;

import java.io.BufferedReader;



import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import api.IDivvyTripsManager;
import api.IVertice;
import au.com.bytecode.opencsv.CSVReader;
import model.vo.VODistance;
import model.vo.VOInterseccion;

import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.HashTables.HashTable;
import model.data_structures.grafo1.ArcoNoExisteException;
import model.data_structures.grafo1.ArcoYaExisteException;
import model.data_structures.grafo1.Grafo;
import model.data_structures.grafo1.VerticeNoExisteException;
import model.data_structures.grafo1.VerticeYaExisteException;
import model.data_structures.listas.LinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	/**
	 * EARTH_RADIUS
	 */
	private static final int EARTH_RADIUS = 6371;


	private LinkedList<VOStation> stations;

	private Grafo<Integer, IVertice<Integer>, VODistance> grafo1;
	private int arcosMixtos;
	private int verticesEstacion;



	public DivvyTripsManager() {
		super();
		this.stations = new LinkedList<VOStation>();
		grafo1=new Grafo<Integer, IVertice<Integer>, VODistance>();
		arcosMixtos=0;
		verticesEstacion=0;
	}



	public Grafo<Integer, IVertice<Integer>, VODistance> getGrafo1() {
		return grafo1;
	}

	public int getArcosMixtos() {
		return arcosMixtos;
	}

	public int getVerticesEstacion() {
		return verticesEstacion;
	}

	public void loadStations (String stationsFile) {
		String csvFile = stationsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			DateFormat format = new SimpleDateFormat("MM/dd/YYYY hh:mm:ss");
			String[] datos;
			while ((line = br.readLine()) != null) {                
				datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {

					stations.addAtEnd(new VOStation(Integer.parseInt(datos[0])+321374, datos[1], datos[2], Double.parseDouble(datos[3]), Double.parseDouble(datos[4]), Integer.parseInt(datos[5]), datos[6]));

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}


	public void loadIntersection (String IntersectionFile) {
		String csvFile = IntersectionFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			String[] datos;
			VOInterseccion i1;
			while ((line = br.readLine()) != null) {                
				datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {

					i1=new VOInterseccion(Integer.parseInt(datos[0]),Double.parseDouble(datos[2]), Double.parseDouble(datos[1]));

					grafo1.addVertex(Integer.parseInt(datos[0]), i1);



				} catch (NumberFormatException e) {

					e.printStackTrace();
				} catch (VerticeNoExisteException e) {

					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	/**
	 * Carga los arcos
	 * @param tripsFile
	 */
	public void loadEdges (String edgesFile) {
		String csvFile = edgesFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = " ";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			line = br.readLine();
			line = br.readLine();


			int idInic=0;
			int idFinal=0;

			VOInterseccion interInic= null;
			VOInterseccion interFinal= null;
			VODistance  arco = null;

			String[] datos;

			double distance;

			while ((line = br.readLine()) != null) {  

				datos = line.split(cvsSplitBy);
				idInic=Integer.parseInt(datos[0]);
				interInic= (VOInterseccion) grafo1.getInfoVertex(idInic);


				for (int i = 1; i < datos.length; i++) {

					idFinal=Integer.parseInt(datos[i]);
					interFinal= (VOInterseccion) grafo1.getInfoVertex(idFinal);
					distance = distance(interInic.getLatitud(), interInic.getLongitud(), interFinal.getLatitud() ,interFinal.getLongitud() );

					arco= new VODistance(distance, interInic, interFinal);

					grafo1.addEdge(idInic, idFinal, arco);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ArcoNoExisteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	/**
	 * Complete el grafo anterior con todas las estaciones de bicicleta del sistema Divvy.
	 * Para tal fin, cada estaci�n deber� ser agregada como un v�rtice tipo estaci�n 
	 * y se construir� un arco entre este nuevo v�rtice estaci�n y el v�rtice intersecci�n
	 * m�s cercano (por distancia harvesiana) del grafo inicial. 
	 * El arco contiene la distancia harvesiana entre sus dos nodos.
	 */
	public void addStationsToGraph() {

		HashTable<Integer, IVertice<Integer>> intersecciones= grafo1.getVertices();
		

		double lat=0;
		double lon=0;

		Iterator<VOStation> it= stations.iterator();
		VOStation station;
		VOInterseccion interseccionActual=null;
		VOInterseccion interseccionMasCercana=null;
		double distaciaMenor=Integer.MAX_VALUE;
		VODistance arcoActual = null;
		double distanciaActual;

		while (it.hasNext()) {
			station =  it.next();

			try {
				grafo1.addVertex(station.darId()+321373, station);
			} catch (VerticeNoExisteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			verticesEstacion++;
			lat=station.darLatitude();
			lon=station.darLongitude();
			interseccionActual=null;
			interseccionMasCercana=null;
			distaciaMenor=Integer.MAX_VALUE;
			arcoActual = null;

			Iterator<Integer> iter= intersecciones.keysIterator();
			
			while (iter.hasNext()) {
				//System.out.println(it2.next());
				//System.out.println(grafo1.getVertices().sizeKeys());
				System.out.println(iter.next());
				interseccionActual= (VOInterseccion) intersecciones.get(iter.next());
				
				
				distanciaActual= distance(lat, lon, interseccionActual.getLatitud(), interseccionActual.getLongitud());
				
				if(distanciaActual<distaciaMenor) {
					distaciaMenor=distanciaActual;
					interseccionMasCercana=interseccionActual;
					
				}
				System.out.println(distaciaMenor);
			}

			System.out.println("louis");
			
			arcoActual=new VODistance(distaciaMenor, interseccionMasCercana, station);

			try {
				grafo1.addEdge(station.darId(), interseccionMasCercana.darId(), arcoActual);
				arcosMixtos++;

			} catch (ArcoNoExisteException e) {
				arcosMixtos--;
				e.printStackTrace();
			}
		}
	}

	//==========================================================
	//GUARDAR GRAFO JSON
	//==========================================================

	/**
	 * Guarda el grafo en formato JSon en el archivo que pasa por parametro
	 * @param ruta La ruta donde se desea guardar el archivo
	 */
	public void guardarGrafoJSON(String ruta) {

		JSONObject js = darJsonObjectDeGrafo(grafo1);

		try {
			FileWriter file = new FileWriter(ruta);
			js.writeJSONString(file);
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo auxiliar para guardar el grafo en un archivo
	 */
	@SuppressWarnings("unchecked")
	private JSONObject darJsonObjectDeGrafo(Grafo<Integer, IVertice<Integer>, VODistance> grafo) {
		JSONObject jsGrafo = new JSONObject(); //principal

		JSONArray jsStationsVertices = new JSONArray();
		JSONArray jsIntercecionesVertices = new JSONArray();
		estacionesYIntersecionesVertices(grafo, jsStationsVertices, jsIntercecionesVertices);
		JSONArray jsArcos = darArcos(grafo);

		jsGrafo.put("numVertices", grafo.V());
		jsGrafo.put("estaciones", jsStationsVertices);
		jsGrafo.put("intersecciones", jsIntercecionesVertices);
		jsGrafo.put("arcos", jsArcos);

		return jsGrafo;
	}

	/**
	 * Metodo auxiliar para guardar el grafo en un archivo
	 */
	@SuppressWarnings("unchecked")
	private JSONArray darArcos(Grafo<Integer, IVertice<Integer>, VODistance> grafo) {
		JSONArray js = new JSONArray();

		LinkedList<VODistance> lista = grafo.darArcos();
		Iterator it = lista.iterator();
		JSONObject arcoJSON;
		VODistance arco = null;

		while(it.hasNext()) {
			arco = (VODistance) it.next();
			arcoJSON = new JSONObject();
			arcoJSON.put("distancia", arco.darPeso());
			arcoJSON.put("id_Vertice_1", arco.getVertice1().darId());
			arcoJSON.put("id_Vertice_2", arco.getVertice2().darId());
			js.add(arcoJSON);
		}

		return js;
	}


	/**
	 * Metodo auxiliar para guardar el grafo en un archivo
	 */
	@SuppressWarnings("unchecked")
	private void estacionesYIntersecionesVertices(Grafo<Integer, IVertice<Integer>, VODistance> grafo, JSONArray estaciones, JSONArray intersecciones) {

		LinkedList<IVertice<Integer>> lista = grafo.darVertices();

		Iterator it = lista.iterator();
		JSONObject verticeAdd;

		while(it.hasNext()) {
			IVertice verticeActual = (IVertice) it.next();

			if(verticeActual instanceof VOInterseccion) {
				verticeAdd = darJsObjectInterseccion((VOInterseccion)verticeActual);			
				intersecciones.add(verticeAdd);
			}
			else if(verticeActual instanceof VOStation) {
				verticeAdd = darJsObjectStation((VOStation)verticeActual);
				estaciones.add(verticeAdd);

			}
		}
	}

	/**
	 * Metodo auxiliar para guardar el grafo en un archivo
	 */
	@SuppressWarnings("unchecked")
	private JSONObject darJsObjectStation(VOStation verticeActual) {
		JSONObject js = new JSONObject();

		js.put("id", verticeActual.darId());
		js.put("name", verticeActual.darName());
		js.put("city", verticeActual.darCity());
		js.put("latitud", verticeActual.darLatitude());
		js.put("longitude", verticeActual.darLongitude());
		js.put("capacity", verticeActual.darDpcapacity());
		js.put("onlineDate", verticeActual.darOnline_date());

		return js;
	}

	/**
	 * Metodo auxiliar para guardar el grafo en un archivo
	 */
	@SuppressWarnings("unchecked")
	private JSONObject darJsObjectInterseccion(VOInterseccion vertice) {
		JSONObject js = new JSONObject();

		js.put("id", vertice.darId());
		js.put("latitud", vertice.getLatitud());
		js.put("longitude", vertice.getLongitud());

		return js;
	}

	//==========================================================
	//CARGAR GRAFO JSON
	//==========================================================
	/**
	 * Carga el grafo del mundo del archivo que se pasa por parametro
	 * @param ruta La ruta del archivo que se desea cargar
	 */
	public void cargarGrafoJSON(String jsonRoute) {

		FileReader file;
		try {
			file = new FileReader(jsonRoute);
			JSONParser parser = new JSONParser();
			JSONObject js = (JSONObject) parser.parse(file);

			grafo1 = darGrafoDeJSonObject(js, grafo1);
			file.close();
		} catch (IOException | ParseException | VerticeYaExisteException | VerticeNoExisteException | ArcoYaExisteException e) {

			e.printStackTrace();
		}
	}

	private Grafo<Integer, IVertice<Integer>, VODistance> darGrafoDeJSonObject(JSONObject js, Grafo<Integer, IVertice<Integer>, VODistance> grafo) throws VerticeYaExisteException, VerticeNoExisteException, ArcoYaExisteException {

		JSONArray inters = (JSONArray) js.get("intersecciones");
		JSONArray estaciones = (JSONArray) js.get("estaciones");
		JSONArray arcos = (JSONArray) js.get("arcos");
		cargarInterseccionesAGrafo(inters, grafo);
		cargarEstacionesAGrafo(estaciones, grafo);
		cargarArcosAGrafo(arcos, grafo);

		return grafo;
	}

	private void cargarArcosAGrafo(JSONArray arcos, Grafo<Integer, IVertice<Integer>, VODistance> grafo) throws VerticeNoExisteException, ArcoYaExisteException {
		VODistance a = null;
		JSONObject obj;
		for(int i = 0; i < arcos.size(); i++) {
			obj = (JSONObject) arcos.get(i);
			cargarArcoAGrafo(obj, grafo, a);
		}
	}

	private void cargarArcoAGrafo(JSONObject obj, Grafo<Integer, IVertice<Integer>, VODistance> grafo, VODistance a) throws VerticeNoExisteException, ArcoYaExisteException {

		Double distancia = (Double) obj.get("distancia");
		int vertice1 = (int) (long) obj.get("id_Vertice_1");
		int vertice2 = (int) (long) obj.get("id_Vertice_2");
		a = new VODistance(distancia, grafo.getInfoVertex(vertice1), grafo.getInfoVertex(vertice2));
		grafo.addEdge(vertice1, vertice2, a);
	}

	private void cargarEstacionesAGrafo(JSONArray estaciones, Grafo<Integer, IVertice<Integer>, VODistance> grafo) throws VerticeYaExisteException {

		VOStation vo = null;
		for(int i = 0; i < estaciones.size(); i++) {
			JSONObject obj = (JSONObject) estaciones.get(i);
			cargarEstacionAGrafo(obj, grafo, vo);
		}
	}

	private void cargarEstacionAGrafo(JSONObject obj, Grafo<Integer, IVertice<Integer>, VODistance> grafo, VOStation vo) throws VerticeYaExisteException {

		int id =  (int) (long) obj.get("id");
		String name =  (String) obj.get("name");
		String city = (String) obj.get("city");
		double latitud =  (double) obj.get("latitud");
		double longitude =  (double) obj.get("longitude");
		String onlineDate =  (String) obj.get("onlineDate");
		int capacity =  (int) (long) obj.get("capacity");

		vo = new VOStation(id, name, city, latitud, longitude, capacity, onlineDate);
		grafo.addVertex(vo);
	}

	private void cargarInterseccionesAGrafo(JSONArray inters, Grafo<Integer, IVertice<Integer>, VODistance> grafo) throws VerticeYaExisteException {

		VOInterseccion vo = null;
		for(int i = 0; i < inters.size(); i++) {
			JSONObject obj = (JSONObject) inters.get(i);
			cargarInterseccionAGrafo(obj, grafo, vo);
		}
	}

	private void cargarInterseccionAGrafo(JSONObject obj, Grafo<Integer, IVertice<Integer>, VODistance> grafo, VOInterseccion vo) throws VerticeYaExisteException {

		double latitud = (double) obj.get("latitud");
		double longitud = (double) obj.get("longitude");
		long id =  (long) obj.get("id");
		Integer id1 = (int) id;
		vo = new VOInterseccion(id1, latitud, longitud);
		grafo.addVertex(vo);
	}


	//==========================================================
	//METODOS JXMAPS
	//==========================================================




	//==========================================================
	//METODOS AUXILIARES
	//==========================================================

	/** 
	 * 
	 * @param startLat
	 * @param startLong
	 * @param endLat
	 * @param endLong
	 * @return
	 */
	public double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}
	/**
	 * Metodos Privados----------------------------------------------
	 */

	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}



}